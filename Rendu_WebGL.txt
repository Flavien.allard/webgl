Bonjour Monsieur, 

Voici donc le projet de WebGL de notre groupe : ALLARD Flavien, BLANCHET Maxime (groupe 4)

Notre projet contient donc 3 objectifs sur 4 demandés. 
Les contraintes des murs, les déplacements ainsi que les ajouts de panneaux avec leur apparition quand nous nous approchons sont gérés.
Nous n'avons pas réussi à gérer une autre caméra dans notre projet malgré que nous ayons réussi à l'ajouter au projet. Caméra que nous avons retirer car non utilisable.

L'objectif de notre petit jeu est donc de rallumer la lumière dans la cathédrale, pour ceci il faudra suivre les instructions et trouver les formes cachées.

La première étape est de trouvé la sphère au rez-de-chaussée, il faut donc comprendre les options de déplacement qui sont : 
-Z pour avancer, Q pour aller à gauche, S pour reculer et D pour aller à droite.
-Les flèches directionnelles pour tourner la tête.

Lorsque vous  aurez trouvé la sphère (dans le coin gauche devant vous de la cathédrale) vous pouvez alors vous retourner et vous avancer un peu pour trouver le premier indice.
Ce sera donc le moment pour vous de voler ! Regardez donc en l'air et appuyez sur la touche pour avancer afin de décoller.
Un autre indice apparaîtra pour vous indiquer qu'il faut trouver la forme de cette étage qui est un cube vert. 

Quand vous aurez trouvé le cube (pas très loin sur la gauche) il faudra rentrer dedans pour obtenir le dernier indice !

La dernière étape est donc de monter jusqu'au plafond et de le traverser. Et nous vous laissons découvrir la suite ...

